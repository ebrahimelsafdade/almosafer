(function($) {
  "use strict";

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 57)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Collapse now if page is not at top
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  navbarCollapse();
  $(window).scroll(navbarCollapse);
    
    
   // carousel 
    function moveToSelected(element) {

    if (element == "next") {
    var selected = $(".selected").next();
    } else if (element == "prev") {
    var selected = $(".selected").prev();
    } else {
    var selected = element;
    }

    var next = $(selected).next();
    var prev = $(selected).prev();
    var nextSecond = $(next).next();
    var nextThird = $(next).next().next();

    $(selected).removeClass().addClass("selected carouselItem");

    $(prev).removeClass().addClass("prev carouselItem");
    $(next).removeClass().addClass("next carouselItem");

    $(nextSecond).removeClass().addClass("nextRightSecond carouselItem");
    $(nextThird).removeClass().addClass("nextRightThird carouselItem");
    $(nextThird).nextAll().removeClass().addClass('hideRight');

    }


    $('#prev').click(function() {
      moveToSelected('prev');
    });

    $('#next').click(function() {
      moveToSelected('next');
    });  

    
    // Scroll reveal calls
    window.sr = ScrollReveal();
        sr.reveal('.credit-card-1', {
        duration: 500,
        delay: 500
    });
        sr.reveal('.credit-card-2', {
        duration: 500,
        delay: 400
    });
        sr.reveal('.credit-card-3', {
        duration: 500,
        delay: 300
    });
        sr.reveal('.credit-card-4', {
        duration: 500,
        delay: 200
    });

    
    

})(jQuery);
