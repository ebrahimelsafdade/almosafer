# Almosafer

Book Cheap flights and Airline tickets, Plan your next unforgettable trip & get cheap flight tickets and Hotels with best airfares on almosafer.

## Getting Started

Get the latest sample code from Bitbucket using Git or download the repository as a ZIP file.
([Download](https://bitbucket.org/ebrahimelsafdade/almosafer.git))

```
$git clone https://bitbucket.org/ebrahimelsafdade/almosafer

```

## Built With

* [Bootstrap](https://getbootstrap.com/) - The web framework used
* [Fontawesome](https://fontawesome.com/) - Font icon library used
* [jQuery](https://jquery.com/) - JavaScript library
* [scrollreveal](https://github.com/jlmakes/scrollreveal) - Scroll animation plugin


## Authors

* **Ebrahim Elsafade** [EbrahimElsafade](https://bitbucket.org/ebrahimelsafdade)



